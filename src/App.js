import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';
import Dashboard from './components/dashboard/Dashboard';
import {Helmet} from "react-helmet";
import ProjectDetails from './components/projects/ProjectDetails';
import SignIn from './components/auth/SignIn';
import SignUp from './components/auth/SignUp';
import CreateProject from './components/projects/CreateProject';
import AdList from './components/dashboard/AdList';
import BlogList from './components/blog/BlogList';
import PrivacyPolicy from './components/blog/PrivacyPolicy';

import RynokVtorichnykhStroymaterialov from './components/blog/RynokVtorichnykhStroymaterialov'
import MacadamDealers from './components/blog/MacadamDealers'
import RoadPlatesDealers from './components/blog/RoadPlatesDealers'
import WasteDisposal from './components/blog/WasteDisposal'
import TonarRent from './components/blog/TonarRent'
import SaltDealers from './components/blog/SaltDealers'
import CrumbDealers from './components/blog/CrumbDealers'
import GraniteMacadamPrice from './components/blog/GraniteMacadamPrice'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Helmet>
            <title>Вторичные стройматериалы</title>
            <meta name="description" content="Доска объявлений для поиска вторичных стройматериалов"/> 
            <meta property="og:title" content="Стройматериалы б/у – ecozuber.ru"/>
          </Helmet>
          <Navbar />
          <Switch>
            <Route exact path='/' component={Dashboard} />
            <Route path='/adlist' component={AdList} />
            <Route path='/project/:id' component={ProjectDetails} />
            <Route path='/signin' component={SignIn} />
            <Route path='/signup' component={SignUp} />
            <Route path='/create' component={CreateProject} />
            <Route exact path='/blog' component={BlogList} />
            <Route exact path='/privacy' component={PrivacyPolicy} />

            <Route exact path='/blog/rynok-vtorichnykh–stroymaterialov' component={RynokVtorichnykhStroymaterialov} />
            <Route exact path='/blog/prodavtsy-shchebnya' component={MacadamDealers} />
            <Route exact path='/blog/prodavtsy-plit' component={RoadPlatesDealers} />
            <Route exact path='/blog/vyvoz-musora' component={WasteDisposal} />
            <Route exact path='/blog/arenda-tonara' component={TonarRent} />
            <Route exact path='/blog/prodavtsy-soli' component={SaltDealers} />
            <Route exact path='/blog/prodavtsy-kroshki' component={CrumbDealers} />
            <Route exact path='/blog/tsena-granitnogo-shchebnya' component={GraniteMacadamPrice} />
          </Switch>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
