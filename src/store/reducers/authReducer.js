const initState = {
  authError: null
}

const authReducer = (state = initState, action) => {
  switch(action.type){
    case 'LOGIN_ERROR':
      return {
        ...state, 
        authError: 'Попробуйте ввести пароль еще раз'
      }
    case 'LOGIN_SUCCESS':
      console.log('Авторизация выполнена');
      return {
        ...state,
        authError: null
      }
    case 'SIGNOUT_SUCCESS':
      console.log('Деавторизация выполнена успешно');
      return state;
    case 'SIGNUP_SUCCESS':
      console.log('Регистрация выполнена успешно');
      return {
        ...state,
        authError: null
      }
    case 'SIGNUP_ERROR':
      console.log('Во время регистрации произошла ошибка');
      return {
        ...state,
        authError: action.error.message
      }
    default: 
      return state;
  }
}

export default authReducer;