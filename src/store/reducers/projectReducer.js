const initState = {
  projects: [
    {id: 1, title: 'Асфальт', content: 'Blah Blah Blah'},
    {id: 2, title: 'Бетон', content: 'Blah Blah Blah'},
    {id: 3, title: 'Кирпич', content: 'Blah Blah Blah'}
  ]
}

const projectReducer = (state = initState, action) => {
  switch (action.type) {
    case 'CREATE_PROJECT':
      console.log('created project', action.project);
      return state;
    case 'CREATE_PROJECT_ERROR':
      console.log('create project error', action.err);
      return state;
    default: 
      return state;
  }
}

export default projectReducer;