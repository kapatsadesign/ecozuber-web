import React, { Component } from 'react';
import algoliasearch from 'algoliasearch/lite';
import {
  InstantSearch,
  Hits,
  SearchBox,
  Pagination,
  Highlight,
  ClearRefinements,
  RefinementList,
  Configure,
} from 'react-instantsearch-dom';
import PropTypes from 'prop-types';
import './App.css';

const searchClient = algoliasearch(
  'ZL7V6958NY',
  '6ccdb3835e97ced1dc5429d1f7c723a5'
);

class App extends Component {
  render() {
    return (
      <div className="ais-InstantSearch">
        <h1>React InstantSearch e-commerce demo</h1>
        <InstantSearch indexName="macadam_dealers" searchClient={searchClient}>
          <div className="left-panel">
            <ClearRefinements />
            <h2>Фракции</h2>
            <RefinementList attribute="macadam_dealers" />
            <Configure hitsPerPage={8} />
          </div>
          <div className="right-panel">
            <div className="md-form form-lg">
              <input type="text" className="form-control form-control-lg" />
              <label htmlFor="inputLGEx">Large input</label>
            </div>
            <SearchBox />
            <Hits hitComponent={Hit} />
            <Pagination />
          </div>
        </InstantSearch>
      </div>
    );
  }
}


function Hit(props) {
  return (
    <div>
      <div className="hit-name">
        <Highlight attribute="name" hit={props.hit} />
      </div>
      <div className="hit-phone">
        <Highlight attribute="phone" hit={props.hit} />
      </div>
      <div className="hit-price">${props.hit.price}</div>
    </div>
  );
}

Hit.propTypes = {
  hit: PropTypes.object.isRequired,
};

export default App;
