import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Initialize Firebase
var config = {
  apiKey: "AIzaSyD4ZGExIrJXQGK5w6JMjIK3F-3XQj8gqqI",
  authDomain: "ecozuber-7a887.firebaseapp.com",
  databaseURL: "https://ecozuber-7a887.firebaseio.com",
  projectId: "ecozuber-7a887",
  storageBucket: "ecozuber-7a887.appspot.com",
  messagingSenderId: "1006171926351"
};
firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;