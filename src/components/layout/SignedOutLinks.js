import React from 'react';
import { NavLink } from 'react-router-dom';

const SignedOutLinks = () => {
  return (
    <div className="collapse navbar-collapse">
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <NavLink to='/' className="btn btn-primary btn-link">Главная</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to='/adlist' className="btn btn-primary btn-link">Объявления</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to='/blog' className="btn btn-primary btn-link">Блог</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to='/signin' className="btn btn-primary btn-link">Войти</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to='/signup' className="btn btn-primary btn-link">Зарегистрироваться</NavLink>
        </li>
      </ul>
    </div>
  )
}

export default SignedOutLinks;