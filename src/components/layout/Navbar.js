import React from 'react';
import SignedInLinks from './SignedInLinks';
import SignedOutLinks from './SignedOutLinks';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const Navbar = (props) => {

  const { auth } = props;
  // console.log(auth);
  const links = auth.uid ? <SignedInLinks /> : <SignedOutLinks />;
  return (
  <nav className="navbar navbar-expand-lg bg-rose">
    <div className="container">
      <div className="navbar-translate">
        <Link to='/'>
          <img className="rounded-circle img-fluid" src='https://s3.eu-central-1.amazonaws.com/zuber-app/white-logo.svg' style={{ height: '60px', padding: '10px' }} alt="Фавикон Ecozuber – Зубр в расцвете сил"/>
        </Link>
      </div>
      { links }
    </div>
  </nav>
  )
}

const mapStateToProps = (state) => {
  console.log(state);
  return{
    auth: state.firebase.auth
  }
}

export default connect(mapStateToProps)(Navbar)