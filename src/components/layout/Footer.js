import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="footer" data-background-color="black">
        <div className="container">
          <nav className="float-left">
            <ul>
              <li>
                <a href="/">
                  Главная
                </a>
              </li>
              <li>
                <a href="/adlist">
                  Объявления
                </a>
              </li>
              <li>
                <a href="/blog">
                  Блог
                </a>
              </li>
              <li>
                <a href="/privacy">
                  Конфиденциальность
                </a>
              </li>
            </ul>
          </nav>
          <div className="copyright float-right">Ecozuber © 2016 - 2019</div>
        </div>
      </footer>
    );
  }
}

export default Footer;