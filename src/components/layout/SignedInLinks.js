import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { signOut } from '../../store/actions/authActions';

const SignedInLinks = (props) => {
  return (
    <div className="collapse navbar-collapse">
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <NavLink to='/create' className="btn btn-primary btn-link">Опубликовать</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to='/' className="btn btn-primary btn-link">Главная</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to='/adlist' className="btn btn-primary btn-link">Объявления</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to='/blog' className="btn btn-primary btn-link">Блог</NavLink>
        </li>
        <li className="nav-item">
          <button className="btn btn-primary btn-link" onClick={props.signOut}>Выйти</button>
        </li>
      </ul>
    </div>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut())
  }
}

export default connect(null, mapDispatchToProps)(SignedInLinks)