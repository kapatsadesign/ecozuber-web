import React, { Component } from 'react';
import Notifications from './Notifications';
import ProjectList from '../projects/ProjectList';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import {Helmet} from "react-helmet";

class AdList extends Component {
  render(){
    const { projects, notifications } = this.props;

    return (
      <div>
        <Helmet>
          <title>Вторичные стройматериалы: объявления</title>
          <meta name="description" content="Список объявлений о вторичных строительных материалах"/> 
          <meta property="og:title" content="Стройматериалы б/у – ecozuber.ru"/>
        </Helmet>
        <div className="dashboard container">
            <div className="col-md-8 ml-auto mr-auto">
              <h1 className="title text-center">Объявления</h1>
            </div>
            <div className="row">
              <div className="col s12 m6">
                <ProjectList projects={projects}/>
              </div>
              <div className="col s12 m5 offset-m1">
                <Notifications notifications={notifications}/>
              </div>
            </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    projects: state.firestore.ordered.projects,
    auth: state.firebase.auth,
    notifications: state.firestore.ordered.notifications
  }
}

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'projects', orderBy: ['createdAt', 'desc'] },
    { collection: 'notifications', limit: 3, orderBy: ['time', 'desc']}
  ])
)(AdList)