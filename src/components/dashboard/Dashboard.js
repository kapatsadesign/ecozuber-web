import React, { Component } from 'react';
import Notifications from './Notifications';
import ProjectList from '../projects/ProjectList';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import {Helmet} from "react-helmet";

const images = { 
  coin: require('./images/coin.png'),
  gear: require('./images/gear.png'),
  tree: require('./images/tree.png'),
  crane: require('./images/crane.png')
};

class Dashboard extends Component {
  render(){
    // console.log(this.props);
    const { projects, notifications } = this.props;

    return (
      <div>
        <Helmet>
          <meta name="description" content="Доска объявлений для поиска вторичных стройматериалов"/> 
          <meta property="og:title" content="Стройматериалы б/у – ecozuber.ru"/>
        </Helmet>
        <div className="container">
          <div className="section text-center">
            <div className="row">
              <div className="col-md-8 ml-auto mr-auto">
                <h1 className="title">Ecozuber βeta</h1>
                <h4>
                  <strong>Отходы в доходы</strong>
                </h4>
              </div>
            </div>
            <div className="card-deck">
              <div className="card mb-4">
                <div className="view overlay">
                  <img className="card-img-top" src={images.coin} alt="Монета" />
                  <a href="#!">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
                <div className="card-body">
                  <h4 className="card-title">Доходно</h4>
                  <p className="card-text">В разы дешевле, чем на рынке первичных материалов</p>
                </div>
              </div>
              <div className="card mb-4">
                <div className="view overlay">
                  <img className="card-img-top" src={images.tree} alt="Дерево" />
                  <a href="#!">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
                <div className="card-body">
                  <h4 className="card-title">Экологично</h4>
                  <p className="card-text">Переиспользуйте и приносите пользу окружающей среде</p>
                </div>
              </div>
              <div className="card mb-4">
                <div className="view overlay">
                  <img className="card-img-top" src={images.gear} alt="Шестеренка" />
                  <a href="#!">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
                <div className="card-body">
                  <h4 className="card-title">Просто</h4>
                  <p className="card-text">Интуитивно понятный интерфейс</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <section>
            <h2 className="my-5 h3 text-center" style={{fontWeight: 700}}>Ваши преимущества</h2>
            <div className="row features-small mb-5 mt-3 wow fadeIn">
              <div className="col-md-4">
                <div className="row">
                  <div className="col-2">
                    <i className="fa fa-check-circle fa-2x indigo-text" />
                  </div>
                  <div className="col-10">
                    <h4>Бесплатно</h4>
                    <p className="grey-text">Работайте с сервисом на своих условиях
                    </p>
                    <div style={{height: 15}} />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <i className="fa fa-check-circle fa-2x indigo-text" />
                  </div>
                  <div className="col-10">
                    <h4>Индивидуально</h4>
                    <p className="grey-text">Глубокий поиск по Вашим запросам</p>
                    <div style={{height: 15}} />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <i className="fa fa-check-circle fa-2x indigo-text" />
                  </div>
                  <div className="col-10">
                    <h4>Минималистично</h4>
                    <p className="grey-text">Работа без лишних затрат на освоение системы</p>
                    <div style={{height: 15}} />
                  </div>
                </div>
              </div>
              <div className="col-md-4 flex-center">
                <img src={images.crane} alt="Строительный кран" className="z-depth-0 img-fluid" />
              </div>
              <div className="col-md-4 mt-2">
                <div className="row">
                  <div className="col-2">
                    <i className="fa fa-check-circle fa-2x indigo-text" />
                  </div>
                  <div className="col-10">
                    <h4>Автоматизированно</h4>
                    <p className="grey-text">Заполните свой профиль, и система "продвинет" Вас 
                    </p>
                    <div style={{height: 15}} />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <i className="fa fa-check-circle fa-2x indigo-text" />
                  </div>
                  <div className="col-10">
                    <h4>Разнообразно</h4>
                    <p className="grey-text">Данные собираются сразу с нескольких источников.
                    </p>
                    <div style={{height: 15}} />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <i className="fa fa-check-circle fa-2x indigo-text" />
                  </div>
                  <div className="col-10">
                    <h4>Полноценно</h4>
                    <p className="grey-text">Услуги-сателлиты (грузоперевозки, документооборот)</p>
                    <div style={{height: 15}} />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>

        <div className="dashboard container">
            <div className="row">
              <div className="col s12 m6">
                <ProjectList projects={projects}/>
              </div>
              <div className="col s12 m5 offset-m1">
                <Notifications notifications={notifications}/>
              </div>
            </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    projects: state.firestore.ordered.projects,
    auth: state.firebase.auth,
    notifications: state.firestore.ordered.notifications
  }
}

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'projects', orderBy: ['createdAt', 'desc'] },
    { collection: 'notifications', limit: 3, orderBy: ['time', 'desc']}
  ])
)(Dashboard)