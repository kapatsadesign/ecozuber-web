import React from 'react';
import moment from 'moment';

const Notifications = (props) => {
  const {notifications} = props;
  return (
    <div className="col-md-6">
      <div className="card">
        <div className="card-body">
          <div className="card-content">
            <h5 className="card-title">Уведомления</h5>
              { notifications && notifications.map(item => {
                return (
                  <ul class="list-group list-group-flush" key={item.id}>
                    <li class="list-group-item">
                      <span className="text-info">{item.user} | {moment(item.time.toDate()).format('L')} </span>
                      <span>{item.content}</span>
                    </li>
                  </ul>
                )
              })}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Notifications;