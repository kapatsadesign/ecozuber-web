import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn } from '../../store/actions/authActions';
import { Redirect } from 'react-router-dom';
import {Helmet} from "react-helmet";

class SignIn extends Component {
  state = {
    email: '',
    password: ''
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.signIn(this.state)
  }
  render() {
    const { authError, auth } = this.props;
    if (auth.uid) return <Redirect to='/' />

    return (
      <div className="col-lg-8 col-12 mt-1 mx-lg-4">
        <Helmet>
          <title>Авторизация</title>
          <meta name="description" content="Войти в личный кабинет ecozuber.ru"/> 
          <meta property="og:title" content="Стройматериалы б/у – ecozuber.ru"/>
        </Helmet>
        <section className="extra-margins pb-5  text-lg-left">
          <div className="row mb-4">
            <div className="col-md-12">
              <div className="container">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title">
                      <strong>Авторизация</strong>
                    </h4>
                    <form className="white" onSubmit={this.handleSubmit}>
                      <div className="form-group bmd-form-group">
                        <label htmlFor="email">Email</label>
                        <input className="form-control" type="email" id="email" onChange={this.handleChange}/>
                      </div>
                      <div className="form-group bmd-form-group">
                        <label htmlFor="password">Пароль</label>
                        <input className="form-control" type="password" id="password" onChange={this.handleChange}/>
                      </div>
                      <div className="input-field">
                        <button className="btn btn-primary btn-round">Пустите меня</button>
                        <div className="text-danger">
                          { authError ? <p>{authError}</p> : null }
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return{
    authError: state.auth.authError,
    auth: state.firebase.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signIn: (creds) => dispatch(signIn(creds))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)