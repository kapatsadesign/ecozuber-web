import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { signUp } from '../../store/actions/authActions';
import {Helmet} from "react-helmet";

class SignUp extends Component {
  state = {
    email: '',
    password: '',
    firstName: '',
    phone: ''
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.signUp(this.state)
  }
  render() {
    const { auth, authError } = this.props;
    if (auth.uid) return <Redirect to='/' />

    return (
      <div className="col-lg-8 col-12 mt-1 mx-lg-4">
        <Helmet>
          <title>Регистрация на ecozuber.ru</title>
          <meta name="description" content="Зарегистрироваться на ecozuber.ru"/> 
          <meta property="og:title" content="Стройматериалы б/у – ecozuber.ru"/>
        </Helmet>
        <section className="extra-margins pb-5  text-lg-left">
          <div className="row mb-4">
            <div className="col-md-12">
              <div className="container">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title">
                      <strong>Регистрация</strong>
                    </h4>
                    <form className="white" onSubmit={this.handleSubmit}>
                      <div className="form-group bmd-form-group">
                        <label htmlFor="email">Email</label>
                        <input className="form-control" type="email" id="email" onChange={this.handleChange}/>
                      </div>
                      <div className="form-group bmd-form-group">
                        <label htmlFor="password">Пароль</label>
                        <input className="form-control" type="password" id="password" onChange={this.handleChange}/>
                      </div>
                      <div className="form-group bmd-form-group">
                        <label htmlFor="firstName">Имя</label>
                        <input className="form-control" type="text" id="firstName" onChange={this.handleChange}/>
                      </div>
                      <div className="form-group bmd-form-group">
                        <label htmlFor="phone">Телефон</label>
                        <input className="form-control" type="text" id="phone" onChange={this.handleChange}/>
                      </div>
                      <div className="input-field">
                        <button className="btn btn-primary btn-round">Создать аккаунт</button>
                        <div className="text-danger">
                          { authError ? <p>{authError}</p> : null }
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signUp: (creds) => dispatch(signUp(creds))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
