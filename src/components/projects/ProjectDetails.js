import React from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import moment from 'moment';
import {Helmet} from "react-helmet";

const ProjectDetails = (props) => {
  const { project } = props;
  if (project) {
    return (
      <div className="container center">
        <Helmet>
          <title>{ project.title }</title>
          <meta property="og:title" content="Стройматериалы б/у – ecozuber.ru"/>
        </Helmet>

        <div className="col-md-8">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">
                <strong>{ project.title }</strong>
              </h4>
              <small>{moment(project.createdAt.toDate()).format('L')}</small>
              <p>{ project.content }</p>
              <hr />
              <a href="https://mdbootstrap.com/bootstrap-tutorial/" className="btn btn-primary btn-round">{ project.phone }
                <i className="fa fa-graduation-cap ml-2" />
              </a>
            </div>
          </div>
        </div>
      </div>
    )
  } else {
    return (
      <div className="container center">
        <p>Загрузка проекта...</p>
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  // console.log(state);
  const id = ownProps.match.params.id;
  const projects = state.firestore.data.projects;
  const project = projects ? projects[id] : null
  return {
    project: project
  }
}

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'projects' }
  ])
)(ProjectDetails);