import React from 'react';
import ProjectSummary from './ProjectSummary';
import { Link } from 'react-router-dom';
import {Helmet} from "react-helmet";

const ProjectList = ({projects}) => {
  return (
    <section className="text-center">
      <Helmet>
        <title>Вторичные строительные материалы</title>
        <meta name="description" content="Доска объявлений для поиска вторичных стройматериалов"/> 
        <meta property="og:title" content="Стройматериалы б/у – ecozuber.ru"/>
      </Helmet>
      <div className="row">
        { projects && projects.map(project => {
          return (
            <Link to={'/project/' + project.id} key={project.id}>
              <ProjectSummary project={project} />
            </Link>
          )
        })}
      </div>
    </section>
  )
}

export default ProjectList