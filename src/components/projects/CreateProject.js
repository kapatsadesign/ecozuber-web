import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createProject } from '../../store/actions/projectActions';
import { Redirect } from 'react-router-dom';
import {Helmet} from "react-helmet";

class CreateProject extends Component {
  state = {
    title: '',
    content: ''
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    // console.log(this.state);
    this.props.createProject(this.state);
    this.props.history.push('/');
  }
  render() {
    const { auth } = this.props;
    if (!auth.uid) return <Redirect to='/signin' />

    return (
      <div className="col-lg-8 col-12 mt-1 mx-lg-4">
        <Helmet>
          <title>Опубликовать объявление</title>
          <meta name="description" content="Создать новое объявление на ecozuber.ru"/> 
          <meta property="og:title" content="Стройматериалы б/у – ecozuber.ru"/>
        </Helmet>
        <section className="extra-margins pb-5  text-lg-left">
          <div className="row mb-4">
            <div className="col-md-12">
              <div className="container">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title">
                      <strong>Новое объявление</strong>
                    </h4>
                    <form className="white" onSubmit={this.handleSubmit}>
                      <div className="form-group bmd-form-group">
                        <label htmlFor="title">Вид материала</label>
                        <input className="form-control" type="text" id="title" onChange={this.handleChange}/>
                      </div>
                      <div className="md-form">
                        <textarea type="text" id="content" class="md-textarea form-control" rows="2" onChange={this.handleChange}></textarea>
                        <label htmlFor="content" >Описание</label>
                      </div>
                      <div className="input-field">
                        <button className="btn btn-primary btn-round">Опубликовать</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

const MapStateToProps = (state) => {
  return {
    auth: state.firebase.auth
  }
}

const mapDispatchToProps = dispatch => {
  return {
    createProject: (project) => dispatch(createProject(project))
  }
}

export default connect(MapStateToProps, mapDispatchToProps)(CreateProject)
