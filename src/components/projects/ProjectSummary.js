import React from 'react';

const ProjectSummary = ({project}) => {
  return (
    <div className="col-md-6">
      <div className="card">
        <div className="card-body">
          <h4 className="card-title">{ project.title }</h4>
          <p className="card-text">{ project.content }</p>
        </div>
      </div>
    </div>
  )
}

export default ProjectSummary;