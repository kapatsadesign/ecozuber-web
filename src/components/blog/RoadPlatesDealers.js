import React, { Component } from "react";
import algoliasearch from "algoliasearch/lite";
import {
  InstantSearch,
  Highlight,
  Hits,
  SearchBox
} from "react-instantsearch-dom";
import PropTypes from "prop-types";
import "./Algolia.css";
import { Helmet } from "react-helmet";

const searchClient = algoliasearch(
  'ZL7V6958NY',
  '6ccdb3835e97ced1dc5429d1f7c723a5'
);

const pageTitle = "Продавцы дорожных плит";

class RoadPlatesDealers extends Component {
  render() {
    return (
      <div className="container">
        <Helmet>
          <title>{pageTitle}</title>
          <meta
            name="description"
            content={pageTitle}
          />
          <meta
            property="og:title"
            content={pageTitle}
          />
          <meta name="keywords" content="дорожные плиты, плиты дорожные купить, плита дорожная цена, дорожные плиты б у, дорожные плиты бу, плита дорожная пдн, купить дорожные плиты б у, плиты дорожные с доставкой, плиты дорожные б у цена, дорожная плита 18, плита дорожная 2п, дорожные плиты 6, плита дорожная пд, плита дорожная 6000х2000х140, плита дорожная 3, плита дорожная 30 18 30, плита дорожная 30.18, плиты дорожные 1п, купить плиты бу дорожные, купить дорожные плиты б +у, плиты дорожные +с доставкой, плиты дорожные б +у цена, плита дорожная 3" />
        </Helmet>
        <div className="col-md-8 ml-auto mr-auto">
          <h1 className="title text-center">{pageTitle}</h1>
        </div>
        <InstantSearch indexName="road_plates_dealers" searchClient={searchClient}>
          <div>
            <SearchBox />
            <div class="row">
              <div className="col">
                <Hits hitComponent={Hit} />
              </div>
            </div>
          </div>
        </InstantSearch>
      </div>
    );
  }
}

function Hit(props) {
  return (
    
      <div className="card">
        <div className="card-body">
          <h4 className="card-title">
            <strong>
              <Highlight attribute="name" hit={props.hit} />
            </strong>
          </h4>
          <div>
            <i class="material-icons">phone</i> 
            <Highlight attribute="phone" hit={props.hit} />
          </div>
          <div>
            <i class="material-icons">near_me</i> 
            <Highlight attribute="city" hit={props.hit} />
          </div>
          <div>₽{props.hit.price}</div>
          <div>
            <i class="material-icons">autorenew</i> 
            <Highlight attribute="newness" hit={props.hit} />
          </div>
          <div>
            <i class="material-icons">bookmark_border</i> 
            <Highlight attribute="vendorСode" hit={props.hit} />
          </div>  
        </div>
      </div>
  );
}

Hit.propTypes = {
  hit: PropTypes.object.isRequired,
};

export default RoadPlatesDealers;
