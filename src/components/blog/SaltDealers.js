import React, { Component } from "react";
import algoliasearch from "algoliasearch/lite";
import {
  InstantSearch,
  Highlight,
  Hits,
  SearchBox
} from "react-instantsearch-dom";
import PropTypes from "prop-types";
import "./Algolia.css";
import { Helmet } from "react-helmet";

const searchClient = algoliasearch(
  'ZL7V6958NY',
  '6ccdb3835e97ced1dc5429d1f7c723a5'
);

const pageTitle = "Продавцы технической соли";

class SaltDealers extends Component {
  render() {
    return (
      <div className="container">
        <Helmet>
          <title>{pageTitle}</title>
          <meta
            name="description"
            content={pageTitle}
          />
          <meta
            property="og:title"
            content={pageTitle}
          />
          <meta name="keywords" content="соль техническая, купить техническую соль, соль техническая +в мешках, соль техническая галит, соль техническая цена" />
        </Helmet>
        <div className="col-md-8 ml-auto mr-auto">
          <h1 className="title text-center">{pageTitle}</h1>
        </div>
        <InstantSearch indexName="salt_dealers" searchClient={searchClient}>
          <div>
            <SearchBox />
            <div class="row">
              <div className="col">
                <Hits hitComponent={Hit} />
              </div>
            </div>
          </div>
        </InstantSearch>
      </div>
    );
  }
}

function Hit(props) {
  return (
    
      <div className="card">
        <div className="card-body">
          <h4 className="card-title">
            <strong>
              <Highlight attribute="company" hit={props.hit} />
            </strong>
          </h4>
          <div>
            <i class="material-icons">phone</i> 
            <Highlight attribute="phone" hit={props.hit} />
          </div>
          <div>
            <i class="material-icons">language</i> 
            <Highlight attribute="website" hit={props.hit} />
          </div>
          <div>
            <i class="material-icons">near_me</i> 
            <Highlight attribute="city" hit={props.hit} />
          </div>
          <div>₽{props.hit.price}</div>
          <div>
            <i class="material-icons">play_for_work</i> 
            <Highlight attribute="amount" hit={props.hit} /> кг
          </div>
          <div>
            <i class="material-icons">bookmark</i> 
            <Highlight attribute="type" hit={props.hit} />
          </div>  
        </div>
      </div>
  );
}

Hit.propTypes = {
  hit: PropTypes.object.isRequired,
};

export default SaltDealers;
