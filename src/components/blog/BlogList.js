import React, {Component} from "react";
import { Helmet } from "react-helmet";
import EllipsisText from "react-ellipsis-text";

const images = { 
  lightweightConstruction: require('./images/lightweight-construction.jpg'),
  graniteMacadam: require('./images/granite-macadam.jpg'),
  concreteRoadPlates: require('./images/concrete-road-plates.jpg'),
  constructionWaste: require('./images/construction-waste.jpg'),
  tonar: require('./images/tonar.jpg'),
  salt: require('./images/salt.jpg'),
  crumbRubber: require('./images/crumb-rubber.jpg'),
  graniteMountain: require('./images/granite-mountain.jpg')
};

class BlogList extends Component {
  render() {
    return (
      <main className="mt-5 pt-5">
        <Helmet>
          <title>Экоблог</title>
          <meta
            name="description"
            content="Экоблог доски объявлений ecozuber.ru"
          />
          <meta
            property="og:title"
            content="Стройматериалы б/у – ecozuber.ru"
          />
        </Helmet>
        <section className="container">
          <div className="col-md-8 ml-auto mr-auto">
            <h1 className="title text-center">Экоблог</h1>
          </div>
          <section className="pt-4">
            <div className="row mt-3 wow fadeIn">
              <div className="col-lg-5 col-xl-4 mb-4">
                <div className="view overlay rounded z-depth-1">
                  <img
                    src={images.lightweightConstruction}
                    className="img-fluid"
                    alt="Каркас строящегося здания"
                  />
                  <a href="/blog/rynok-vtorichnykh–stroymaterialov" target="_blank" rel="noopener noreferrer">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
              </div>
              <div className="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                <h3 className="mb-3 font-weight-bold dark-grey-text">
                  <strong>Как устроен рынок вторичных стройматериалов в 2019 году</strong>
                </h3>
                <p class="text-left">
                  <EllipsisText text={"Вторичные строительные материалы – это ресурсы, которые переиспользуются для возведения или ремонта зданий и сооружений. Подвергнуто ли сырье"} length={"80"} />
                </p>
                <a
                  href="/blog/rynok-vtorichnykh–stroymaterialov"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn btn-primary btn-md"
                >
                  Читать
                  <i className="fa fa-play ml-2" />
                </a>
              </div>
            </div>
            <hr className="mb-5"/>
          </section>
          <section className="pt-4">
            <div className="row mt-3 wow fadeIn">
              <div className="col-lg-5 col-xl-4 mb-4">
                <div className="view overlay rounded z-depth-1">
                  <img
                    src={images.graniteMacadam}
                    className="img-fluid"
                    alt="Гранитный щебень"
                  />
                  <a href="/blog/prodavtsy-shchebnya" target="_blank" rel="noopener noreferrer">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
              </div>
              <div className="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                <h3 className="mb-3 font-weight-bold dark-grey-text">
                  <strong>Компании, продающие гранитный щебень</strong>
                </h3>
                <p>                   
                  <EllipsisText text={"Стоимость, контактные телефоны и фракция сырья"} length={"80"} />                 
                </p>
                <a
                  href="/blog/prodavtsy-shchebnya"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn btn-primary btn-md"
                >
                  Читать
                  <i className="fa fa-play ml-2" />
                </a>
              </div>
            </div>
            <hr className="mb-5"/>
          </section>
          <section className="pt-4">
            <div className="row mt-3 wow fadeIn">
              <div className="col-lg-5 col-xl-4 mb-4">
                <div className="view overlay rounded z-depth-1">
                  <img
                    src={images.concreteRoadPlates}
                    className="img-fluid"
                    alt="Железобетонные дорожные плиты"
                  />
                  <a href="/blog/prodavtsy-plit" target="_blank" rel="noopener noreferrer">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
              </div>
              <div className="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                <h3 className="mb-3 font-weight-bold dark-grey-text">
                  <strong>База компаний, продающих  дорожные плиты</strong>
                </h3>
                  <p>                   
                    <EllipsisText text={"Стоимость, контактные телефоны, новизна и артикулы дорожных плит"} length={"80"} />                 
                  </p>
                <a
                  href="/blog/prodavtsy-plit"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn btn-primary btn-md"
                >
                  Читать
                  <i className="fa fa-play ml-2" />
                </a>
              </div>
            </div>
            <hr className="mb-5"/>
          </section>
          <section className="pt-4">
            <div className="row mt-3 wow fadeIn">
              <div className="col-lg-5 col-xl-4 mb-4">
                <div className="view overlay rounded z-depth-1">
                  <img
                    src={images.constructionWaste}
                    className="img-fluid"
                    alt="Строительный мусор"
                  />
                  <a href="/blog/vyvoz-musora" target="_blank" rel="noopener noreferrer">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
              </div>
              <div className="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                <h3 className="mb-3 font-weight-bold dark-grey-text">
                  <strong>База компаний, вывозящих строительный мусор</strong>
                </h3>
                <p>                   
                  <EllipsisText text={"Стоимость вывоза и погрузки, контактные телефоны, объем и тоннаж техники"} length={"80"} />                 
                </p>
                <a
                  href="/blog/vyvoz-musora"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn btn-primary btn-md"
                >
                  Читать
                  <i className="fa fa-play ml-2" />
                </a>
              </div>
            </div>
            <hr className="mb-5"/>
          </section>
          <section className="pt-4">
            <div className="row mt-3 wow fadeIn">
              <div className="col-lg-5 col-xl-4 mb-4">
                <div className="view overlay rounded z-depth-1">
                  <img
                    src={images.tonar}
                    className="img-fluid"
                    alt="Универсальный автопоезд Тонар-9540"
                  />
                  <a href="/blog/arenda-tonara" target="_blank" rel="noopener noreferrer">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
              </div>
              <div className="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                <h3 className="mb-3 font-weight-bold dark-grey-text">
                  <strong>База компаний, предоставляющих тонары в аренду</strong>
                </h3>
                <p>                   
                  <EllipsisText text={"Почасовая и посменная стоимость аренды полуприцепов, контактные телефоны и вместимость"} length={"80"} />                 
                </p>
                <a
                  href="/blog/arenda-tonara"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn btn-primary btn-md"
                >
                  Читать
                  <i className="fa fa-play ml-2" />
                </a>
              </div>
            </div>
            <hr className="mb-5"/>
          </section>
          <section className="pt-4">
            <div className="row mt-3 wow fadeIn">
              <div className="col-lg-5 col-xl-4 mb-4">
                <div className="view overlay rounded z-depth-1">
                  <img
                    src={images.salt}
                    className="img-fluid"
                    alt="Техническая соль"
                  />
                  <a href="/blog/prodavtsy-soli" target="_blank" rel="noopener noreferrer">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
              </div>
              <div className="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                <h3 className="mb-3 font-weight-bold dark-grey-text">
                  <strong>База компаний, продающих техническую соль</strong>
                </h3>
                <p>                   
                  <EllipsisText text={"Стоимость, контактные телефоны, фасовочные объемы и типы соли"} length={"80"} />                 
                </p>
                <a
                  href="/blog/prodavtsy-soli"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn btn-primary btn-md"
                >
                  Читать
                  <i className="fa fa-play ml-2" />
                </a>
              </div>
            </div>
            <hr className="mb-5"/>
          </section>
          <section className="pt-4">
            <div className="row mt-3 wow fadeIn">
              <div className="col-lg-5 col-xl-4 mb-4">
                <div className="view overlay rounded z-depth-1">
                  <img
                    src={images.crumbRubber}
                    className="img-fluid"
                    alt="Вторичная асфальтно-резиновая крошка"
                  />
                  <a href="/blog/prodavtsy-kroshki" target="_blank" rel="noopener noreferrer">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
              </div>
              <div className="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                <h3 className="mb-3 font-weight-bold dark-grey-text">
                  <strong>База компаний, продающих асфальтную крошку</strong>
                </h3>
                <p>                   
                  <EllipsisText text={"Стоимость, контактные телефоны, фракция асфальтной крошки и минимальный объем поставки"} length={"80"} />                 
                </p>
                <a
                  href="/blog/prodavtsy-kroshki"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn btn-primary btn-md"
                >
                  Читать
                  <i className="fa fa-play ml-2" />
                </a>
              </div>
            </div>
            <hr className="mb-5"/>
          </section>
          <section className="pt-4">
            <div className="row mt-3 wow fadeIn">
              <div className="col-lg-5 col-xl-4 mb-4">
                <div className="view overlay rounded z-depth-1">
                  <img
                    src={images.graniteMountain}
                    className="img-fluid"
                    alt="Гранитные скалы, США"
                  />
                  <a href="/blog/tsena-granitnogo-shchebnya" target="_blank" rel="noopener noreferrer">
                    <div className="mask rgba-white-slight" />
                  </a>
                </div>
              </div>
              <div className="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                <h3 className="mb-3 font-weight-bold dark-grey-text">
                  <strong>Статистика цен гранитного щебня</strong>
                </h3>
                <p>                   
                  <EllipsisText text={"Какая цена считается низкой, а какая высокой? Какая фракция самая ходовая?"} length={"80"} />                 
                </p>
                <a
                  href="/blog/tsena-granitnogo-shchebnya"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn btn-primary btn-md"
                >
                  Читать
                  <i className="fa fa-play ml-2" />
                </a>
              </div>
            </div>
            <hr className="mb-5"/>
          </section>
        </section>
      </main>
    );
  }
}

export default BlogList;
