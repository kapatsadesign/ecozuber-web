import React, {Component} from "react";
import { Helmet } from "react-helmet";
// import ReactImageMagnify from 'react-image-magnify';

const macadamPriceStats = require('./images/macadam-price.jpg');
const graniteMountain = require('./images/granite-mountain.jpg');
const pageTitle = "Статистика цен гранитного щебня в 2019 г.";

class macadamPrice extends Component {
  render() {
    return (
      <div className="container center">
        <Helmet>
          <title>{pageTitle}</title>
          <meta
            name="description"
            content={pageTitle}
          />
          <meta
            property="og:title"
            content={pageTitle}
          />
          <meta name="keywords" content="щебень гранитный фр, щебень гранитный, купить щебень цена, купить щебень цена доставка, щебень гранитный цена, щебень гранитный доставка цена" />
        </Helmet>
        <div className="col-md-8">
          <div className="card mb-4 wow fadeIn animated">
            <img src={graniteMountain} className="img-fluid" alt="Гранитные скалы"/>
          </div>
          <div className="card">
            <div className="card-body">
              <h1 className="title">{pageTitle}</h1>
              <p>Ecozuber провел для Вас специальное исследование с целью выяснить, какая цена гранитного щебня на рынке Санкт-Петербурга считается низкой, а какая – высокой. В выборку попали 94 объявления из популярных агрегаторов (avito.ru, blizko.ru), а также публикации на официальных сайтах производителей стройсырья. Цены приведены к единице измерения "рубли за тонну" согласно таблице объемных весов гранитного щебня (<a href="https://naruservice.com/articles/udelnyj-ves-shchebnya">naruservice.com</a>).</p>
              <img className="carousel" src={macadamPriceStats} alt="График цен гранитного щебня"/>
              <small>Нажмите "Открыть изображение в новой вкладке, чтобы увеличить</small>
              <p>Итак, некоторые интересные выводы:</p>
              <p>– 100% продавцов из представленной выборки осуществляют доставку с помощью собственного автопарка.</p>
              <p>– Самая дорогая фракция гранитного щебня – 3 - 10 мм (в среднем 1568 рублей за тонну).</p>
              <p>– Низкой ценой считается диапазон 150 - 700 рублей за тонну (первая квартиль выборки).</p>
              <p>– Средней ценой считается диапазон 893 - 1 100 рублей за тонну (вторая и третья квартили).</p>
              <p>– Высокой ценой считается диапазон 1 101 - 2 000 рублей за тонну (четвертая квартиль).</p>
              <p>В выборке представлено 16 разновидностей гранитного щебня, из которых самыми ходовыми считаются семь: 0 - 5, 5 - 10, 5 - 20, 20 - 40, 25 - 60, 40 - 70, 70 - 250 мм. В свою очередь, чемпионами по распространенности в данной подгруппе являются следуюшие фракции: 20 - 40, 40 - 70.</p>
              <p>Самыми активными игроками на территории СЗФО являются 14 компаний-производителей: "Неруд-база Юг", "Профбетон", "Максимум", "Содружество", "Конкорд-бетон Северо-Запад", "Конкорд Питер Снаб", "Профиль", "Профлогистик", "Линпром", "Фасовкин", "Щебторг", "Арго" и "Песковичок".</p>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default macadamPrice;