import React, { Component } from "react";
import algoliasearch from "algoliasearch/lite";
import {
  InstantSearch,
  Highlight,
  Hits,
  SearchBox
} from "react-instantsearch-dom";
import PropTypes from "prop-types";
import "./Algolia.css";
import { Helmet } from "react-helmet";

const searchClient = algoliasearch(
  'ZL7V6958NY',
  '6ccdb3835e97ced1dc5429d1f7c723a5'
);

const pageTitle = "База компаний, вывозящих строительный мусор";

class WasteDisposal extends Component {
  render() {
    return (
      <div className="container">
        <Helmet>
          <title>{pageTitle}</title>
          <meta
            name="description"
            content={pageTitle}
          />
          <meta
            property="og:title"
            content={pageTitle}
          />
          <meta name="keywords" content="купить щебень, щебень гранитный, купить щебень +с доставкой, купить щебень цена, купить щебень гранитный, щебень 20 купить, купить щебень цена доставка, вторичный щебень, щебень гранитный цена, купить щебень 40, щебень гранитный 5 20, щебень гранитный 20 40, щебень гранитный +с доставкой, щебень гранитный доставка цена, где купить щебень, щебень гранитный цена +за куб, строительный мусор, вывоз строительного мусора, пухто, вывоз мусора строительного мусора цена, вывожу строительный мусор, вывезти строительный мусор, вывоз пухто, пухто спб, вывоз строительного мусора контейнер, грузчики строительный мусор, вывоз мусора пухто, вынос строительного мусора, вывоз строительного мусора +с грузчиками, строительный мусор +из квартиры, вывоз строительного мусора дешево, уборка строительного мусора, утилизация строительного мусора" />
        </Helmet>
        <div className="col-md-8 ml-auto mr-auto">
          <h1 className="title text-center">{pageTitle}</h1>
        </div>
        <InstantSearch indexName="waste_disposal" searchClient={searchClient}>
          <div>
            <SearchBox />
            <div class="row">
              <div className="col">
                <Hits hitComponent={Hit} />
              </div>
            </div>
          </div>
        </InstantSearch>
      </div>
    );
  }
}

function Hit(props) {
  return (
    
      <div className="card">
        <div className="card-body">
          <h4 className="card-title">
            <strong>
              <Highlight attribute="company" hit={props.hit} />
            </strong>
          </h4>
          <div>
            <i class="material-icons">phone</i> 
            <Highlight attribute="phone" hit={props.hit} />
          </div>
          <div>
            <i class="material-icons">language</i> 
            <Highlight attribute="website" hit={props.hit} />
          </div>
          <div>
            <i class="material-icons">near_me</i> 
            <Highlight attribute="city" hit={props.hit} />
          </div>
          <div>₽{props.hit.price}</div>
          <div>
            <i class="material-icons">play_for_work</i> 
            ₽<Highlight attribute="shipmentPrice" hit={props.hit} />
          </div>
          <div>
            <i class="material-icons">filter_frames</i> 
            <Highlight attribute="volume" hit={props.hit} />
          </div>  
          <div>
            <i class="material-icons">lens</i> 
            <Highlight attribute="weight" hit={props.hit} />
          </div>  
          <div>
            <i class="material-icons">bookmark</i> 
            <Highlight attribute="carModel" hit={props.hit} />
          </div>  
        </div>
      </div>
  );
}

Hit.propTypes = {
  hit: PropTypes.object.isRequired,
};

export default WasteDisposal;
