## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Creates a production build of full value. You also need to clear dist folder, and put freshly created build from 'build'.

### and then `firebase deploy`

Deploys React project to firebase to technical domain. If common domain is configured in 'Hosting', the website is completely updated!
